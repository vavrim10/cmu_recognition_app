# VISIONAID: Seeing beyond the screen.

## Popis
Toto je aplikace napsaná pro CMU na rozpoznávání a ukládaní dat z obrazovek digitálních měřicích přístrojů.

Aplikace je napsaná v jazyce Python. Zpracování obrázků je realizováno knihovnou [OpenCV](https://github.com/opencv/opencv). Grafické rozhraní je vykreslováno knihovnou [CustomTkinter](https://github.com/TomSchimansky/CustomTkinter). O samotné rozpoznávání hodnot se stará program [Tesseract](https://tesseract-ocr.github.io/tessdoc/).

Ke správnému fungování aplikace je potřeba videokamera, která je umístěná na nepohyblivém místě. Oprazovka přístroje by měla být rovnoměrně osvětlená tak, aby nedocházelo k odrážení světla od obrazovky. Aplikace následně zpracovává živý obraz z kamery, na který se aplikuje základní image processing. V nastaveném intervalu jsou snímky předávány k rozpoznání hodnoty, která je následně uložena do souboru ve formátu ".csv"

## Instalace

- [ ] Nejprve je nutné nainstalovat [Tesseract](https://tesseract-ocr.github.io/tessdoc/Downloads.html)
- [ ] Po instalaci je třeba vložit soubor "ssd.traineddata" (ve složce traineddata) do systému tesseractu, například:

```
C:\Program Files\Tesseract-OCR\tessdata\ssd.traineddata
```

- [ ] Po instalaci Tesseractu a natrénovaných dat je možné nainstalovat rozpoznávací aplikaci VISIONAID
- [ ] Po instalaci je třeba vložit cesty k Tesseractu a k traineddata do konfiguračního souboru "config.ini", například takto:

```
[path to tesseract]
tessdata_prefix = C:\\Program Files\\Tesseract-OCR\\tessdata
path_to_exe = C:\\Program Files\\Tesseract-OCR\\tesseract.exe
...
```

- [ ] V souboru "config.ini" je možné kdykoliv před spuštěním aplikace změnit nastavení kamery, dále se do tohoto souboru ukládá i uživatelem nastavená konfigurace aplikace. Soubor "config.ini" naleznete v adresáři VISIONAID spolu s exe souborem.

```
...
[Camera]
frame_width = 640
frame_height = 480
camera_brightness = 150
...
```

## Ovládání

- [ ] V prostřední části aplikace se nachází výstup z kamery, který je zobrazován po aplikaci obrazových úprav. Tento obraz je ve zvolených intervalech předáván k vyhodnocení.

- [ ] V levém ovládacím panelu se nachází nastavení obrazu.
 - Výběr připojené kamery - aktivní pouze při zastaveném obrazu - (může se stát, že je na výběr více než 1 připojená kamera, po výběru nesprávné kamery a spuštění přehrávání však obraz "neběží" - nutno zvolit jinou kameru)
 - Play / stop videa - po zastavení obrazu nedochází k aktualizaci obrázku kamerou.
 - Crop obrazu myší - aktivní pouze při zastaveném obrazu - cr. 1 (výběr 1. bodu), cr. 2 (výběr 2. bodu), crop (oříznout obraz podle bodů 1 a 2)
 - Dodatečné oříznutí obrazu (slidery: Up, Down, Left, Right)
 - Velikost obrazu - slider Scale - toto nastavení je pouze pro uživatele a nemění žádný parametr / chování týkající se rozpoznání obrázku (rozpoznání je prováděno nehledě na hodnotě scale)
 - Uprava obrazu (slidery: threshold, contrast, blur, transform, checkbox: invert) - aktivní pouze po aktivování checkboxu "Threshold On / Off"
 - Slider Transform - narovnává šikmý obraz - určeno především k narovnání šikmého řezu písma na rovné písmo
 - Invert - invertuje obraz (bílá -> černá...)
 - Reset - resetuje veškeré nastavení
 - System / Dark / Light - uživatelský barevný mód okna programu

- [ ] V pravém ovládacím panelu se nachází nastavení rozpoznávání.
 - Load configuration - načte uloženou konfiguraci ze systémového souboru "config.ini", tento soubor je možné přenest z jiného pracoviště.
 - Save configuration - uloží právě nastavenou konfiguraci do souboru "config.ini"
 - Browse - výběr cesky pro ukládání výstupních dat, jméno výstupního souboru se zadává pod "Browse" do entryboxu
 - Try tu recognize dot - Program se sám snaží najít kromě čísel i desetinou tečku / čárku. Možné změnit za "dot after first" atd., program pak vysloveně tečku hledat nebude a vloží ji za 1. symbol.
 - Normal digit tess. - Režim rozpoznávání hodnot: normální "lidké symboly". Možné změnit na: 7 seg. digits tess. (7 segmentové číselné displeje), 7 seg. digits analytic (7 segmentové číselné displeje - rozpoznání řešeno matematickým popisem, neurčuje tečku)
 - Tag - poznámka, která se napíše na konec řádku ve výstupním souboru, viz výstup z programu.
 - Time interval - nastavení časového intervalu rozpoznávání / ukládání hodnot (po sekundách)
 - Recognition - zahájení rozpoznávání hodnot v časovém intervalu, stále lze měnit nastavení, data se nikam neukládají
 - Save data - zahájení ukládání dat, některé nastavení se uzamkne (aby uživatel ho nechtěně nemohl změnit)

## ukázky správných / špatných vstupů

## Výstup z programu

Výstupem z programu je tabulka "output.csv", případně jiný uživatelem zadaný název souboru. Obsah souboru může být následující:

```
year,month,day,hour,minute,second,value,tag
2023,04,15,14,44,17,100.45,None
2023,04,15,14,44,18,100.45,None
2023,04,15,14,44,19,100.45,None
2023,04,15,14,44,20,100.45,None
2023,04,15,14,44,21,100.45,None
2023,04,15,14,44,22,100.45,None
2023,04,15,14,44,23,100.45,None
2023,04,15,14,44,24,100.45,None
2023,04,15,14,44,26,100.45,None
2023,04,15,14,44,27,100.45,None
```

Kde "value" je rozpoznaná hodnota z obrazovky přístroje a "tag" je uživatelem napsaná poznámka, například: "uA" ,"V", "ms", "měřil Jan Novák", "přístroj 1", ...


## Project status

- [ ] Plně funkční první verze je připravená k testování. Zbývá dodělat instalační balíček.
- [ ] Pokud se vyskytne nějaký bug, prosím o nahlášení.

## Plány do budoucna

- [ ] Rozpoznávání více oblastí jednou aplikací najednou
- [ ] Rozpoznávání indikačních ledek na přístroji

## Autoři
- [ ] Tento program vytvořil Bc. Marek Vavřínek pod vedením doc Ing. Stanislava Vítka Ph.D., ČVUT FEL


