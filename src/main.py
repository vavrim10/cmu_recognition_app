######################### CMU recognition app #########################
################# Created by Bc. Marek Vavřínek, 2023 #################
#   Supervisor: doc. Ing. Stanislav Vítek Ph.D, ČVUT FEL              #
#######################################################################

#######################################################################
#### import libraries
#######################################################################
import customtkinter
from tkinter import filedialog
from tkinter import Tk
from PIL import Image, ImageTk
import os
import cv2
import time
import pandas as pd
import pytesseract
import numpy as np
from imutils.perspective import four_point_transform
from ocr_7seg import OCR_7seg
import configparser

def returnCameraIndexes():
    # Získání seznamu dostupných kamer
    camera_list = []
    for i in range(10): # Otestuje kameru s ID od 0 do 9
        cap = cv2.VideoCapture(i, cv2.CAP_DSHOW)
        if cap.isOpened():
            camera_list.append("webcam "+str(i))
            cap.release()
    return camera_list

def setCaptureCam(index,frameWidth,frameHeight,brightness):
    cap =  cv2.VideoCapture(index, cv2.CAP_DSHOW)
    cap.set(3, frameWidth)
    cap.set(4, frameHeight)
    cap.set(10, brightness)        # brightness
    return cap

def histStretch(img, value):          # roztazeni histogramu
    in_points = [0,64,128,255] # control points of input intensities
    out_points = [0,0,255,255] # control points of output intensities
    in_vals = np.arange(256)   # lin spaced value 0:255
    # automatic in points
    perc_shadows = value
    perc_highlight = 100 - value
    in_points = [0, np.percentile(img, perc_shadows), np.percentile(img, perc_highlight), 255]
    # 2 options of LUT
    lut_shape = np.interp(in_vals,in_points,out_points).astype('uint8') # create LUT
    #gamma_val = 2             # Gamma LUT krivka
    #lut_shape = np.uint8(255*(in_vals/255)**gamma_val)
    img_lut = cv2.LUT(img, lut_shape)
    return img_lut

def load_config_bool(config, sekce, hodnota):
    value = config.get(sekce, hodnota)
    if value.lower() == 'true' or value == '1' or value.lower() == 'yes':
        value = True
    else:
        value = False
    return value

def write_config_bool(hodnota):
    if hodnota: return "True"
    else: return "False"

def load_sectret_conf(value):
    config = configparser.ConfigParser()
    config.read('config.ini')
    if value == "exe": return config.get('path to tesseract', 'path_to_exe')
    elif value == "prefix": return config.get('path to tesseract', 'TESSDATA_PREFIX')
    elif value == "width": return int(config.get('Camera', 'frame_width'))
    elif value == "height": return int(config.get('Camera', 'frame_height'))
    elif value == "brightness": return int(config.get('Camera', 'camera_brightness'))
    else:
        print("wrong variable to choose")
        return ""

class App(customtkinter.CTk):
    #############################################################################################
    ### Tesseract configuration:
    #############################################################################################
    # cesta k tesseractu, musi byt nainstalovany
    tess_path_to_exe = load_sectret_conf("exe")
    pytesseract.pytesseract.tesseract_cmd = tess_path_to_exe
    TESSDATA_PREFIX = load_sectret_conf("prefix")
    #pytesseract.pytesseract.tesseract_cmd = "C:\\Program Files\\Tesseract-OCR\\tesseract.exe"
    #TESSDATA_PREFIX = "C:\\Program Files\\Tesseract-OCR\\tessdata"
    config_tess = "--psm 8 -c tessedit_char_whitelist=+-.0123456789" # aby tesseract hledal pouze cisla
    dot_order = 0                                                    # pozice tečky - 0 = nepridavat tecku
    # oem 0 - rychle a mala presnost
    # oem 1 - lepsi presnost, ale pomale
    # oem 2 - kombinace obojiho - nejlepsi presnost
    # oem 3 - default
    # psm - ruzne konfigurece: 6 -> predpokladej 1 uniformni blok textu (Default)
    # psm 7 - img je 1 radek textu, 8 - img je 1 slovo, 5 - uniformni blok vertikalne (1 radek?)
    #  6    Assume a single uniform block of text.
    # 7    Treat the image as a single text line.
    # 8    Treat the image as a single word.
    
    time_interval = 1000                        # only: x * 1000 - recognition trigger - [ms]
    fps_interval = 50                           # video capture - frame interval - [ms]
    last_recognised_value = "None"
    filepath = "."
    tag_value = "None"

    #########################################################################################
    #### setup camera
    #########################################################################################
    frameWidth = load_sectret_conf("width")
    frameHeight = load_sectret_conf("height")
    cam_brightness = load_sectret_conf("brightness")
    listOfCams = returnCameraIndexes()
    cap = setCaptureCam(int(listOfCams[0].split()[-1]),frameWidth,frameHeight,cam_brightness)
    success, img_cap = cap.read()
    if success: img = img_cap
    img = cv2.cvtColor(img,cv2.COLOR_BGR2RGB)   # prevod na RGB (cv2 bere obr jako BGR)

    ###### TEST IMAGE - this have to be commented
    #img = cv2.imread('pristroj/4.jpg')              # nacteni obrazku
    #img = cv2.cvtColor(img,cv2.COLOR_BGR2RGB)   # prevod na RGB (cv2 bere obr jako BGR)
    
    ########################################################################################
    ##############   image processing variables
    ########################################################################################
    img_dimensions = img.shape
    max_up = img_dimensions[0]
    max_right = img_dimensions[1]
    Up = 0
    Down = max_up
    Left = 0
    Right = max_right
    Threshold = 127
    blur_kernel = 1
    transform_value = 0
    imgGray = cv2.cvtColor(img[Up:Down,Left:Right], cv2.COLOR_RGB2GRAY)
    imgBlur = cv2.GaussianBlur(imgGray,(5,5),0)
    img_threshold = cv2.threshold(imgBlur,Threshold,255,cv2.THRESH_BINARY)
    img_to_be_rec = cv2.cvtColor(img[Up:Down,Left:Right], cv2.COLOR_RGB2GRAY)
    cropX1 = 0
    cropY1 = 0
    cropX2 = max_right
    cropY2 = max_up
    cropAct = False

    ##############################################################################################################################
    ####################    Function definicion
    ##############################################################################################################################
    def load_configuration(self):
        config = configparser.ConfigParser()
        config.read('config.ini')
        self.cropX1 = int(config.get('variables', 'cropX1'))
        self.cropY1 = int(config.get('variables', 'cropY1'))
        self.cropX2 = int(config.get('variables', 'cropX2'))
        self.cropY2 = int(config.get('variables', 'cropY2'))
        self.cropAct = load_config_bool(config, 'variables', "cropAct")
        self.Up = int(config.get('variables', 'Up'))
        self.Down = int(config.get('variables', 'Down'))
        self.Left = int(config.get('variables', 'Left'))
        self.Right = int(config.get('variables', 'Right'))
        self.Threshold = int(config.get('variables', 'Threshold'))
        self.slider_up.set(int(config.get('variables', 'slider_up')))
        self.slider_down.set(int(config.get('variables', 'slider_down')))
        self.slider_left.set(int(config.get('variables', 'slider_left')))
        self.slider_right.set(int(config.get('variables', 'slider_right')))
        self.slider_threshold.set(int(config.get('variables', 'slider_threshold')))
        self.slider_contrast.set(float(config.get('variables', 'slider_contrast')))
        self.slider_scale.set(float(config.get('variables', 'slider_scale')))
        self.slider_blur.set(int(config.get('variables', 'slider_blur')))
        self.slider_transform.set(int(config.get('variables', 'slider_transform')))
        self.transform_value = self.slider_transform.get()
        if load_config_bool(config, 'variables', "checkbox_threshold"): self.checkbox_thres.select()
        else: self.checkbox_thres.deselect()
        if load_config_bool(config, 'variables', "checkbox_invert"): self.checkbox_invert.select()
        else: self.checkbox_invert.deselect()
        
        self.switch_saveData.deselect()
        self.saveData_disable_function()
        self.switch_saveData.configure(state="disabled")
        self.switch_recognition.deselect()
        self.crop_update_image()
        self.treshold_disable_function()
    
    def save_configuration(self):
        nazev_sekce = 'variables'
        hodnoty = {
        'cropX1': str(self.cropX1),
        'cropY1': str(self.cropY1),
        'cropX2': str(self.cropX2),
        'cropY2': str(self.cropY2),
        'cropAct': str(self.cropAct),
        'Up': str(int(self.Up)),
        'Down': str(int(self.Down)),
        'Left': str(int(self.Left)),
        'Right': str(int(self.Right)),
        'Threshold': str(self.Threshold),
        'slider_up': str(int(self.slider_up.get())),
        'slider_down': str(int(self.slider_down.get())),
        'slider_left': str(int(self.slider_left.get())),
        'slider_right': str(int(self.slider_right.get())),
        'slider_threshold': str(int(self.slider_threshold.get())),
        'slider_contrast': str(self.slider_contrast.get()),
        'slider_scale': str(self.slider_scale.get()),
        'slider_blur': str(int(self.slider_blur.get())),
        'slider_transform': str(int(self.slider_transform.get())),
        'checkbox_invert': write_config_bool(self.checkbox_invert.get()),
        'checkbox_threshold': write_config_bool(self.checkbox_thres.get()),
        }
        soubor = 'config.ini'
        config = configparser.ConfigParser()
        config.add_section('path to tesseract')
        config.set('path to tesseract', 'TESSDATA_PREFIX', self.TESSDATA_PREFIX)
        config.set('path to tesseract', 'path_to_exe', self.tess_path_to_exe)
        config.add_section('Camera')
        config.set('Camera','frame_width', str(self.frameWidth))
        config.set('Camera','frame_height', str(self.frameHeight))
        config.set('Camera','camera_brightness', str(self.cam_brightness))
        config[nazev_sekce] = hodnoty
        with open(soubor, 'w') as configfile:
            config.write(configfile)
            configfile.close()

    def motion(self,event):
        if (self.segmented_button_crop.get() == "set cr. 1") & self.cropAct:
            mouseX1, mouseY1 = event.x , event.y
            self.cropX1 = int(self.max_right* mouseX1 / self.home_frame_image.winfo_width())
            self.cropY1 = int(self.max_up* mouseY1 / self.home_frame_image.winfo_height())
            img_tmp = self.img.copy()
            img_tmp = cv2.rectangle(img_tmp, (int(self.cropX1),int(self.cropY1)),(int(self.cropX2),int(self.cropY2)),(255,0,0),2)
            self.image.configure(light_image=Image.fromarray(img_tmp),size=(self.max_right,self.max_up))
        if (self.segmented_button_crop.get() == "set cr. 2") & self.cropAct:
            mouseX2, mouseY2 = event.x , event.y
            self.cropX2 = int(self.max_right* mouseX2 / self.home_frame_image.winfo_width())
            self.cropY2 = int(self.max_up* mouseY2 / self.home_frame_image.winfo_height())
            img_tmp = self.img.copy()
            img_tmp = cv2.rectangle(img_tmp, (int(self.cropX1),int(self.cropY1)),(int(self.cropX2),int(self.cropY2)),(255,0,0),2)
            self.image.configure(light_image=Image.fromarray(img_tmp),size=(self.max_right,self.max_up))
        self.cropAct = True
    
    def seg_button_crop_callback(self,value):
        if value == "set cr. 1":
            img_tmp = self.img.copy()
            img_tmp = cv2.rectangle(img_tmp, (int(self.cropX1),int(self.cropY1)),(int(self.cropX2),int(self.cropY2)),(255,0,0),2)
            self.image.configure(light_image=Image.fromarray(img_tmp),size=(self.max_right,self.max_up))
            self.cropAct = False
        elif value == "set cr. 2":
            img_tmp = self.img.copy()
            img_tmp = cv2.rectangle(img_tmp, (int(self.cropX1),int(self.cropY1)),(int(self.cropX2),int(self.cropY2)),(255,0,0),2)
            self.image.configure(light_image=Image.fromarray(img_tmp),size=(self.max_right,self.max_up))
            self.cropAct = False
        elif value == "crop":
            self.Up = self.cropY1
            self.Down = self.cropY2
            self.Left = self.cropX1
            self.Right = self.cropX2
            self.slider_down.set(self.Down)
            self.slider_up.set(self.Up)
            self.slider_right.set(self.Right)
            self.slider_left.set(self.Left)
            self.crop_update_image()
            self.cropAct = False
    
    def threshold_update_image(self):
        pts = np.array([(self.Left-self.transform_value, self.Up), (self.Right-self.transform_value, self.Up), (self.Left, self.Down), (self.Right, self.Down)], dtype = "float32")
        self.imgGray = four_point_transform(self.img, pts)
        self.imgGray = cv2.cvtColor(self.imgGray, cv2.COLOR_RGB2GRAY)
        self.imgGray = histStretch(self.imgGray, self.slider_contrast._output_value)
        self.imgBlur = cv2.GaussianBlur(self.imgGray,(5,5),0)
        ret, self.img_threshold = cv2.threshold(self.imgBlur,self.slider_threshold._output_value,255,cv2.THRESH_BINARY)
        self.img_threshold = cv2.GaussianBlur(self.img_threshold,(self.blur_kernel,self.blur_kernel),0)
        if self.checkbox_invert.get():
            img_inverted = abs(255 - self.img_threshold)
            self.image.configure(light_image=Image.fromarray(img_inverted),
                            size=((self.Right-self.Left)*self.slider_scale._output_value, (self.Down-self.Up)*self.slider_scale._output_value))
            self.img_to_be_rec = img_inverted
        elif (self.checkbox_thres.get()):
            self.image.configure(light_image=Image.fromarray(self.img_threshold),
                            size=((self.Right-self.Left)*self.slider_scale._output_value, (self.Down-self.Up)*self.slider_scale._output_value))
            self.img_to_be_rec = self.img_threshold

    def crop_update_image(self):
        if not self.checkbox_thres.get():
            self.image.configure(light_image=Image.fromarray(self.img[self.Up:self.Down,self.Left:self.Right]),
                                size=((self.Right-self.Left)*self.slider_scale._output_value,
                                        (self.Down-self.Up)*self.slider_scale._output_value))
            self.img_to_be_rec = self.img[self.Up:self.Down,self.Left:self.Right]
        else:
            self.threshold_update_image()

    def change_appearance_mode_event(self, new_appearance_mode):
        customtkinter.set_appearance_mode(new_appearance_mode)

    def button_reset_callback(self):
        self.cropX1 = 0
        self.cropY1 = 0
        self.cropX2 = self.max_right
        self.cropY2 = self.max_up
        self.cropAct = False
        self.Up = 0
        self.Down = self.max_up
        self.Left = 0
        self.Right = self.max_right
        self.Threshold = 127
        self.slider_up.set(0)
        self.slider_down.set(self.max_up)
        self.slider_left.set(0)
        self.slider_right.set(self.max_right)
        self.slider_threshold.set(127)
        self.slider_contrast.set(0)
        self.slider_scale.set(1)
        self.slider_blur.set(0)
        self.slider_transform.set(0)
        self.checkbox_thres.deselect()
        self.checkbox_invert.deselect()
        self.switch_saveData.deselect()
        self.saveData_disable_function()
        self.switch_saveData.configure(state="disabled")
        self.switch_recognition.deselect()
        self.crop_update_image()
        self.treshold_disable_function()

    def treshold_disable_function(self):
        if self.checkbox_thres.get(): state_is = "normal"
        else: state_is = "disabled"
        self.slider_transform.configure(state=state_is)
        self.slider_blur.configure(state=state_is)
        self.slider_threshold.configure(state=state_is)
        self.slider_contrast.configure(state=state_is)
        self.checkbox_invert.configure(state=state_is)

    def saveData_disable_function(self):
        if self.switch_saveData.get(): state_is = "disabled"
        else: state_is = "normal"
        self.switch_recognition.configure(state=state_is)
        self.button_browse.configure(state=state_is)
        self.entry_fileName.configure(state=state_is)
        self.optionmenu_dot.configure(state=state_is)
        self.optionmenu_font.configure(state=state_is)
        self.button_load_config.configure(state=state_is)

    def checkbox_treshold_callback(self):
        self.treshold_disable_function()
        self.crop_update_image()

    def chceckbox_invert_callback(self):
        self.checkbox_treshold_callback()

    def slider_threshold_callback(self, value):
        self.threshold_update_image()

    def slider_contrast_callback(self, value):
        self.threshold_update_image()

    def slider_blur_callback(self, value):
        self.blur_kernel = int(value * 25) * 2 + 1
        self.threshold_update_image()

    def slider_transform_callback(self, value):
        self.transform_value = value
        self.threshold_update_image()

    def slider_Up_callback(self, value):
        if value < self.Down: self.Up = int(value)
        self.crop_update_image()
    
    def slider_Down_callback(self, value):
        if value > self.Up: self.Down = int(value)
        self.crop_update_image()

    def slider_Left_callback(self, value):
        if value < self.Right: self.Left = int(value)
        self.crop_update_image()

    def slider_Right_callback(self, value):
        if value > self.Left: self.Right = int(value)
        self.crop_update_image()

    def slider_scale_callback(self, value):
        self.image.configure(size=((self.Right-self.Left)*value, (self.Down-self.Up)*value))

    def switch_recognition_callback(self):
        self.recognition_trigger()
        if self.switch_recognition.get():
            self.switch_saveData.configure(state="normal")
        else:
            self.switch_saveData.configure(state="disabled")

    def switch_saveData_callback(self):
        filepathname = str(self.filepath)+"/"+self.entry_fileName.get()
        if not os.path.exists(filepathname):
            header = "year,month,day,hour,minute,second,value,tag\n"
            with open(filepathname, 'a', newline='') as file:
                file.writelines(header)
                file.close()
        self.saveData_disable_function()

    def save_data_to_CSV(self):
        t = time.localtime()
        current_time = time.strftime("%Y,%m,%d,%H,%M,%S", t)
        filepathname = str(self.filepath)+"/"+self.entry_fileName.get()
        with open(filepathname, 'a', newline='') as file:
            if self.entry_tag.get() == "": self.tag_value = "None"
            else: self.tag_value = self.entry_tag.get()
            file.writelines(str(current_time)+","+str(self.last_recognised_value)+","+str(self.tag_value)+"\n")
            file.close()

    def button_browse_callback(self):
        self.filepath = filedialog.askdirectory(initialdir = "/")
        filepathname = str(self.filepath)+"/"+self.entry_fileName.get()
        if len(filepathname) > 24:
            self.label_path.configure(text="..."+filepathname[-24:])
        else:
            self.label_path.configure(text=filepathname)
        self.switch_saveData_callback()

    def seg_button_play_callback(self,value):
        if value == "Play video":
            self.optionmenu_webcam.configure(state="disabled")
            self.segmented_button_crop.configure(state="disabled")
            self.video_capture_trigger()

    def optionmenu_webcam_callback(self,value):
        index = int(value.split()[-1])
        self.cap.release()
        self.optionmenu_webcam.configure(values=returnCameraIndexes())   #update připojených kamer
        self.cap = setCaptureCam(index,self.frameWidth,self.frameHeight, self.cam_brightness)
        success, img_cap = self.cap.read()
        if success: img = img_cap
        self.img = cv2.cvtColor(img,cv2.COLOR_BGR2RGB)   # prevod na RGB (cv2 bere obr jako BGR)
        self.crop_update_image()

    def video_capture_trigger(self):
        self.success, self.img_cap = self.cap.read()
        if self.success: self.img = self.img_cap
        self.img = cv2.cvtColor(self.img,cv2.COLOR_BGR2RGB)   # prevod na RGB (cv2 bere obr jako BGR)
        # schedule next update
        if self.segmented_button_play.get() == "Stop video":
            self.optionmenu_webcam.configure(state="normal")
            self.segmented_button_crop.configure(state="normal")
            self.after_cancel(self.timer_FPS)
        else:
            self.timer_FPS = self.after(self.fps_interval, self.video_capture_trigger)
        self.crop_update_image()

    def optionmenu_dot_callback(self, value):
        #"try to recognize dot", "dot after first", "dot after second", "dot after third"
        if value == "try to recognize dot":
            self.config_tess = "--psm 7 -c tessedit_char_whitelist=+-.0123456789"
            self.dot_order = 0
        else:
            self.config_tess = "--psm 7 -c tessedit_char_whitelist=+-0123456789"
            if value == "dot after first":
                self.dot_order = 1
            if value == "dot after second":
                self.dot_order = 2
            if value == "dot after third":
                self.dot_order = 3
            if value == "dot after fourth":
                self.dot_order = 4
            if value == "dot after fifth":
                self.dot_order = 5

    def recognition_trigger(self):
        self.recognition_tesseract()
        if self.switch_saveData.get(): self.save_data_to_CSV()
        if not self.switch_recognition.get():   # schedule next update
            self.after_cancel(self.timer)
        else:
            self.timer = self.after(self.time_interval, self.recognition_trigger)

    def recognition_tesseract(self):
        #["normal digits - tess.", "7 seg. digits - tess.", "7 seg. digits - analytic"]
        if self.optionmenu_font.get() == "normal digits - tess.":
            self.last_recognised_value = pytesseract.image_to_string(self.img_to_be_rec,config=self.config_tess)
        elif self.optionmenu_font.get() == "7 seg. digits - tess.":
            self.last_recognised_value = pytesseract.image_to_string( Image.fromarray(self.img_to_be_rec), lang="ssd", config=self.config_tess)
            #self.last_recognised_value= pytesseract.image_to_string( Image.fromarray(self.img_to_be_rec), lang="letsgodigital", config=self.config_tess)
        elif "7 seg. digits - analytic":
            self.last_recognised_value = OCR_7seg(self.img_to_be_rec)
        self.last_recognised_value = self.last_recognised_value.strip()
        if self.last_recognised_value == "": self.last_recognised_value = "None"
        elif self.dot_order:
            self.last_recognised_value = self.last_recognised_value[:self.dot_order] + "." + self.last_recognised_value[self.dot_order:]
        self.reg_label.configure(text=self.last_recognised_value)
        #print("Tesseract: " + self.last_recognised_value)

    def slider_time_interval_callback(self, value):
        self.label_time_interval.configure(text="time interval: " + str(int(self.slider_time_interval._output_value)) + " s")
        self.time_interval = 1000*int(self.slider_time_interval._output_value)
        #print("time interval: "+ str(int(value)))

    #############################################################################################################################################
    ################            Graphic interface configuration
    #############################################################################################################################################
    def __init__(self):
        super().__init__()
        self.title("VISIONAID: Seeing beyond the screen.")
        self.iconbitmap("images\icon.ico")
        temp_str =  str(self.max_right+500) + "x" + str(700)
        self.geometry(temp_str)
        self.grid_rowconfigure(0, weight=1)
        self.grid_columnconfigure(1, weight=1)

        # Control elements
        self.control = customtkinter.CTkScrollableFrame(self)
        self.control.grid(row=0, column=0, padx=10, pady=10, sticky="nsew")
        self.control.grid_rowconfigure(26, weight=1)

        self.ico =  customtkinter.CTkImage(Image.open("images\\icon.png"), size=(110, 110))
        self.home_frame_image = customtkinter.CTkLabel(master=self.control, text="", image=self.ico)
        self.home_frame_image.grid(row=0, column=0, padx=0, pady=(40,0))

        self.optionmenu_webcam = customtkinter.CTkOptionMenu(master=self.control, values=self.listOfCams, command=self.optionmenu_webcam_callback)
        #self.optionmenu_webcam.set("webcam 0")
        self.optionmenu_webcam.grid(row=1, column=0, padx=(10,20), pady=(50,5), sticky="ew")

        self.segmented_button_play = customtkinter.CTkSegmentedButton(master=self.control, values=["Play video", "Stop video"],
                                                                    command=self.seg_button_play_callback)
        self.segmented_button_play.set("Stop video")  # set initial value
        self.segmented_button_play.grid(row=2, column=0, padx=(10,20), pady=5, sticky="ew")

        self.segmented_button_crop = customtkinter.CTkSegmentedButton(master=self.control, 
                                    values=["set cr. 1", "set cr. 2", "crop"], command=self.seg_button_crop_callback)
        self.segmented_button_crop.set("crop")                  # set initial value
        self.segmented_button_crop.grid(row=3, column=0, padx=(10, 20), pady=(5,30), sticky="ew")

        self.label_up = customtkinter.CTkLabel(master=self.control, text="up")
        self.label_up.grid(row=4, column=0, padx=(10, 20), pady=0, sticky="ew")
        self.slider_up = customtkinter.CTkSlider(master=self.control, from_=0, to=self.max_up,command=self.slider_Up_callback, width=180)
        self.slider_up.grid(row=5, column=0, padx=(10, 20), pady=5, sticky="ew")
        self.slider_up.set(self.Up)

        self.label_down = customtkinter.CTkLabel(master=self.control, text="Down")
        self.label_down.grid(row=6, column=0, padx=(10, 20), pady=(5,0), sticky="ew")
        self.slider_down = customtkinter.CTkSlider(master=self.control, command=self.slider_Down_callback, from_=0, to=self.max_up, width=180)
        self.slider_down.grid(row=7, column=0, padx=(10, 20), pady=5, sticky="ew")
        self.slider_down.set(self.Down)

        self.label_left = customtkinter.CTkLabel(master=self.control, text="left")
        self.label_left.grid(row=8, column=0, padx=(10, 20), pady=(5,0), sticky="ew")
        self.slider_left = customtkinter.CTkSlider(master=self.control, command=self.slider_Left_callback, from_=0, to=self.max_right, width=180)
        self.slider_left.grid(row=9, column=0, padx=(10, 20), pady=5, sticky="ew")
        self.slider_left.set(self.Left)

        self.label_right = customtkinter.CTkLabel(master=self.control, text="right")
        self.label_right.grid(row=10, column=0, padx=(10, 20), pady=(5,0), sticky="ew")
        self.slider_right = customtkinter.CTkSlider(master=self.control, command=self.slider_Right_callback, from_=0, to=self.max_right, width=180)
        self.slider_right.grid(row=11, column=0, padx=(10, 20), pady=5, sticky="ew")
        self.slider_right.set(self.Right)

        self.label_scale = customtkinter.CTkLabel(master=self.control, text="scale")
        self.label_scale.grid(row=12, column=0, padx=(10, 20), pady=(5,0), sticky="ew")
        self.slider_scale = customtkinter.CTkSlider(master=self.control, command=self.slider_scale_callback, from_=1/8, to=3, number_of_steps=23, width=180)
        self.slider_scale.grid(row=13, column=0, padx=(10, 20), pady=5, sticky="ew")
        self.slider_scale.set(1)

        self.label_threshold = customtkinter.CTkLabel(master=self.control, text="threshold")
        self.label_threshold.grid(row=14, column=0, padx=(10, 20), pady=(5,0), sticky="ew")
        self.slider_threshold = customtkinter.CTkSlider(master=self.control, command=self.slider_threshold_callback, from_=1, to=255, width=180)
        self.slider_threshold.grid(row=15, column=0, padx=(10, 20), pady=5, sticky="ew")
        self.slider_threshold.set(127)

        self.label_contrast = customtkinter.CTkLabel(master=self.control, text="contrast")
        self.label_contrast.grid(row=16, column=0, padx=(10, 20), pady=(5,0), sticky="ew")
        self.slider_contrast = customtkinter.CTkSlider(master=self.control, command=self.slider_contrast_callback,from_=0, to=10, number_of_steps=40, width=180)
        self.slider_contrast.grid(row=17, column=0, padx=(10, 20), pady=5, sticky="ew")
        self.slider_contrast.set(0)

        self.label_blur = customtkinter.CTkLabel(master=self.control, text="blur")
        self.label_blur.grid(row=18, column=0, padx=(10, 20), pady=(5,0), sticky="ew")
        self.slider_blur = customtkinter.CTkSlider(master=self.control, command=self.slider_blur_callback, from_=0, to=1, number_of_steps=100, width=180)
        self.slider_blur.grid(row=19, column=0, padx=(10, 20), pady=5, sticky="ew")
        self.slider_blur.set(0)

        self.label_transform = customtkinter.CTkLabel(master=self.control, text="transform")
        self.label_transform.grid(row=20, column=0, padx=(10, 20), pady=(5,0), sticky="ew")
        self.slider_transform = customtkinter.CTkSlider(master=self.control, command=self.slider_transform_callback, from_=-10, to=10, number_of_steps=21, width=180)
        self.slider_transform.grid(row=21, column=0, padx=(10, 20), pady=5, sticky="ew")
        self.slider_transform.set(self.transform_value)

        self.checkbox_thres = customtkinter.CTkCheckBox(master=self.control, text="Threshold On / Off", command=self.checkbox_treshold_callback)
        self.checkbox_thres.grid(row=22, column=0, padx=(15,20), pady=(10,5), sticky="ew")

        self.checkbox_invert = customtkinter.CTkCheckBox(master=self.control, text="Invert On / Off", command=self.chceckbox_invert_callback)
        self.checkbox_invert.grid(row=23, column=0, padx=(15,20), pady=(5,10), sticky="ew")

        self.button_reset = customtkinter.CTkButton(master=self.control, text="Reset", command=self.button_reset_callback)
        self.button_reset.grid(row=24, column=0, padx=(10,20), pady=5, sticky="ew")

        self.appearance_mode_menu = customtkinter.CTkOptionMenu(master=self.control, values=["Light", "Dark", "System"],
                                                                command=self.change_appearance_mode_event)
        self.appearance_mode_menu.set("System")
        self.appearance_mode_menu.grid(row=26, column=0, padx=(15,30), pady=60, sticky="s")


        #####################################################################################################################################
        ########### main image "home" frame
        #####################################################################################################################################
        self.home_frame = customtkinter.CTkFrame(self, corner_radius=0, fg_color="transparent")
        self.home_frame.grid(row=0, column=1, padx=10, pady=10)
        self.home_frame.grid_columnconfigure(1, weight=1)

        self.image = customtkinter.CTkImage(Image.fromarray(self.img[self.Up:self.Down,self.Left:self.Right]),
                                            size=(self.Right-self.Left,self.Down-self.Up))
        self.home_frame_image = customtkinter.CTkLabel(master=self.home_frame, text="", image=self.image)
        self.home_frame_image.grid(row=0, column=0, padx=0, pady=0)


        #####################################################################################################################################
        ########### right navigation frame
        #####################################################################################################################################
        self.navigation = customtkinter.CTkFrame(self)
        self.navigation.grid(row=0, column=2, padx=10, pady=10, sticky="nsew")
        self.navigation.grid_rowconfigure(13, weight=1)

        self.button_load_config = customtkinter.CTkButton(master=self.navigation, text="Load configuration",
                                                        command=self.load_configuration)
        self.button_load_config.grid(row=0, column=0, padx=10, pady=(60,5), sticky="ew")

        self.button_save_config = customtkinter.CTkButton(master=self.navigation, text="Save configuration",
                                                        command=self.save_configuration)
        self.button_save_config.grid(row=1, column=0, padx=10, pady=(5,5), sticky="ew")

        self.button_browse = customtkinter.CTkButton(master=self.navigation, text="Browse", command=self.button_browse_callback)
        self.button_browse.grid(row=2, column=0, padx=10, pady=5, sticky="ew")

        self.entry_fileName = customtkinter.CTkEntry(master=self.navigation, placeholder_text="File name",
                                                    textvariable=customtkinter.StringVar(self, value='Output.csv'))
        self.entry_fileName.grid(row=3, column=0, padx=10, pady=5, sticky="ew")

        self.path_label_frame = customtkinter.CTkFrame(master=self.navigation)
        self.path_label_frame.grid(row=4, column=0, padx=10, pady=(5,30), sticky="nsew")
        self.label_path = customtkinter.CTkLabel(master=self.path_label_frame,text=str(self.filepath)+"/"+self.entry_fileName.get())
        self.label_path.grid(row=1, column=0, padx=10, pady=5, sticky="ew")

        self.optionmenu_dot = customtkinter.CTkOptionMenu(self.navigation, command=self.optionmenu_dot_callback,
                                                    values=["try to recognize dot", "dot after first", "dot after second", "dot after third", "dot after fourth"])
        self.optionmenu_dot.grid(row=5, column=0, padx=10, pady=5, sticky="ew")
        self.optionmenu_dot.set("try to recognize dot")

        self.optionmenu_font = customtkinter.CTkOptionMenu(self.navigation, values=["normal digits - tess.", "7 seg. digits - tess.", "7 seg. digits - analytic"])
        self.optionmenu_font.grid(row=6, column=0, padx=10, pady=5, sticky="ew")
        self.optionmenu_font.set("normal digits - tess.")

        # reg label frame
        self.reg_label_frame = customtkinter.CTkFrame(master=self.navigation)
        self.reg_label_frame.grid(row=7, column=0, padx=10, pady=5, sticky="nsew")
        self.reg_label2 = customtkinter.CTkLabel(master=self.reg_label_frame, text="Last recognised value:")
        self.reg_label2.grid(row=1, column=0, padx=(25,0), pady=(5,0), sticky="ew")
        self.reg_label = customtkinter.CTkLabel(master=self.reg_label_frame, text=self.last_recognised_value)
        self.reg_label.grid(row=2, column=0, padx=(25,0), pady=(0,5), sticky="ew")

        self.entry_tag = customtkinter.CTkEntry(master=self.navigation, placeholder_text="tag")
        self.entry_tag.grid(row=8, column=0, padx=10, pady=5, sticky="ew")

        self.slider_time_interval = customtkinter.CTkSlider(master=self.navigation, command=self.slider_time_interval_callback,
                                                            from_=1, to=30, number_of_steps=29, width=180)
        self.slider_time_interval.grid(row=10, column=0, padx=10, pady=5, sticky="ew")
        self.slider_time_interval.set(self.time_interval/1000)
        self.label_time_interval = customtkinter.CTkLabel(master=self.navigation,
                                                        text="time interval: " + str(int(self.slider_time_interval._output_value))+ " s")
        self.label_time_interval.grid(row=9, column=0, padx=10, pady=5, sticky="ew")

        self.switch_recognition = customtkinter.CTkSwitch(master=self.navigation, text="Recognition", command=self.switch_recognition_callback)
        self.switch_recognition.grid(row=11, column=0, padx=30, pady=(10,5), sticky="ew")

        self.switch_saveData = customtkinter.CTkSwitch(master=self.navigation, text="Save data",
                                                    command=self.switch_saveData_callback, state="disabled")
        self.switch_saveData.grid(row=12, column=0, padx=30, pady=5, sticky="ew")

        ############################################################################
        ### dodatečné prvotní nastavení
        self.treshold_disable_function()

####################################################################################################################################################################
######## Spusteni aplikace
####################################################################################################################################################################
if __name__ == "__main__":
    app = App()                             # vytvoreni applikace
    app.bind('<Button>', app.motion)        # propojeni tlacitka mysi na vyber oblasti kliknutim v obrazku
    app.mainloop()                          # "start programu"
